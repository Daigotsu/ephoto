### Chats:
* https://gitter.im/rus-speaking/android
* https://gitter.im/JavaBy/chat
* https://gitter.im/rus-speaking/android-kotlin

### Butterknife:
* http://jakewharton.github.io/butterknife/

### Kotlin:
* http://kotlinlang.org/docs/reference/null-safety.html

### Useful links:
* http://cyrilmottier.com/2014/09/25/deep-dive-into-android-state-restoration/ (how to handle state change)
* http://android-developers.blogspot.com/2015/05/android-design-support-library.html (magic lib with material design)
* https://github.com/bright/slf4android (standard logger, can write file logs, eg. for offline log usage)
* https://docs.google.com/spreadsheets/d/1-O25YTEuIYltvTPJbamJ7nIKYRQy8gPddRJFKu3R-OU/edit#gid=0 (material design libs)