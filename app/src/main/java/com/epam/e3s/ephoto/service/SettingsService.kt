package com.epam.e3s.ephoto.service

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.epam.e3s.ephoto.R
import com.epam.e3s.ephoto.data.Token
import com.epam.e3s.ephoto.util.json

class SettingsService(val ctx: Context) {
    object Key {
        public val EVENT_TAG_PREF: String = "event_tag_pref"
        public val EVENT_CITY: String = "event_city_pref"
        public val E3S_URL: String = "e3s_url_pref"
        public val E3S_USERNAME: String = "e3s_username_pref"
        public val E3S_PASSWD: String = "e3s_passwd_pref"
    }

    public val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)

    private val token = getToken();

    fun getEventTag() = getString(Key.EVENT_TAG_PREF, "")

    fun getEventCity() = getString(Key.EVENT_CITY, "")

    fun getE3SBaseUrl() = getString(Key.E3S_URL, token.baseUrl)

    fun getE3SUser() = getString(Key.E3S_USERNAME, token.principal)

    fun getE3SPassword() = getString(Key.E3S_PASSWD, token.credentials)

    fun getToken() = json(ctx.getResources(), R.raw.token, javaClass<Token>())

    private fun getString(key: String, default: String): String {
        val result = prefs.getString(key, "")
        if(result.isBlank()) {
            return default
        }
        return result
    }
}
