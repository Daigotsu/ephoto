package com.epam.e3s.ephoto.data

data class Token(val baseUrl: String, val principal: String, val credentials: String)

