package com.epam.e3s.ephoto

import android.app.Fragment
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.epam.e3s.ephoto.data.ItWeekRegistration
import com.squareup.picasso.Callback
import org.apache.commons.codec.binary.Hex
import org.apache.commons.codec.digest.DigestUtils
import retrofit.RetrofitError
import retrofit.client.Response

public class SecurityMainFragment : Fragment() {

    private val TAG = javaClass<SecurityMainFragment>().getName()

    private var ctx: Context? = null

    var fullNameTextView: TextView? = null
    var titleTextView: TextView? = null
    var cityTextView: TextView? = null
    var photoView: ImageView? = null
    var employeeId: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ctx = this.getActivity()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.security_main_fragment, container, false)

        fullNameTextView = view?.findViewById(R.id.employeeFullname) as TextView
        titleTextView = view?.findViewById(R.id.employeeTitle) as TextView
        cityTextView = view?.findViewById(R.id.employeeCity) as TextView
        photoView = view?.findViewById(R.id.employeePhoto) as ImageView
        employeeId = view?.findViewById(R.id.employeeIdInput) as EditText

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null && savedInstanceState.getString("employeeName") != null) {
            fullNameTextView?.setText(savedInstanceState.getString("employeeName"))
            titleTextView?.setText(savedInstanceState.getString("employeeTitle"))
            highlightCity(savedInstanceState.getString("employeeCity"))
            photoView?.setImageBitmap(savedInstanceState.getParcelable("employeePhoto"))
        }

        registerEmployeeIdListener()

        employeeId?.requestFocus()
    }

    private fun registerEmployeeIdListener() {
        val handler = Handler()

        employeeId?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                handler.removeCallbacksAndMessages(null)
            }

            override fun afterTextChanged(s: Editable) {
                if (s.length() > 0) {
                    handler.postDelayed({
                        onEmployeeIdEntered()
                    }, 100)
                }
            }
        })

        employeeId?.setOnKeyListener({ view, keyCode, keyEvent ->
            if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                handler.removeCallbacksAndMessages(null)
                onEmployeeIdEntered()
            }
            false
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("employeeName", fullNameTextView?.getText()?.toString())
        outState.putString("employeeTitle", titleTextView?.getText()?.toString())
        outState.putString("employeeCity", cityTextView?.getText()?.toString())
        outState.putParcelable("employeePhoto", (photoView?.getDrawable() as? BitmapDrawable)?.getBitmap())
    }

    fun getMainActivity() = if (this.isAdded()) this.getActivity() as MainActivity else null
    fun getEventTag() = getMainActivity()?.settingsService?.getEventTag()!!
    fun getEventCity() = getMainActivity()?.settingsService?.getEventCity()!!
    fun getE3SBaseUrl() = getMainActivity()?.settingsService?.getE3SBaseUrl()!!
    fun getE3SService() = getMainActivity()?.e3SRestServiceBuilder?.getE3SRestService()!!
    fun getPicasso() = getMainActivity()?.e3SRestServiceBuilder?.getPicasso(ctx!!)!!
    fun getHash(sid: String?) = String(Hex.encodeHex(DigestUtils.md5(String(Hex.encodeHex(DigestUtils.sha(sid))))))

    private fun onEmployeeIdEntered() {
        val inputSIDText = employeeId?.getText().toString()
        employeeId?.setText("")
        if (inputSIDText.length() > 3) {
            registerEmployee(inputSIDText)
        }
    }

    private fun registerEmployee(sid: String) {
        getE3SService().let { e3sService ->
            e3sService.registerEmployee(getEventTag(), getHash(sid), object : retrofit.Callback<ItWeekRegistration> {
                override fun success(registration: ItWeekRegistration, response: Response) {
                    Log.i(TAG, "Employee data rest: success")

                    if ("OK".equals(registration.status) && registration.employee != null) {
                        renderEmployeeData(registration.employee, registration.alreadyRegistered)

                        if (!registration.alreadyRegistered) {
                            Toast.makeText(ctx, getString(R.string.register_success_toast), Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(ctx, getString(R.string.register_again_toast), Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        restoreDefault()
                        Toast.makeText(ctx, "${getString(R.string.register_failed_toast)}: ${registration.message}", Toast.LENGTH_SHORT).show()
                    }

                    employeeId?.requestFocus()
                }

                override fun failure(error: RetrofitError) {
                    Log.e(TAG, "Employee data rest: failure")

                    if (error.getResponse() != null) {
                        val errorCode = error.getResponse().getReason()
                        Toast.makeText(ctx, "Error on getting employee data: $errorCode", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(ctx, "Error on getting employee data.", Toast.LENGTH_SHORT).show()
                    }
                    employeeId?.requestFocus()
                }
            })
        }
    }

    private fun renderEmployeeData(empl: ItWeekRegistration.Employee, alreadyRegistered: Boolean) {
        fullNameTextView?.setText(empl.name.toUpperCase())
        titleTextView?.setText(empl.title)

        val cityHighlighted = highlightCity(empl.city)

        if (!cityHighlighted && alreadyRegistered) {
            changeBackgroundColor(R.color.attention_color)
        }

        loadEmployeePhoto(empl.photoUrl)
    }

    fun loadEmployeePhoto(url: String) {
        photoView?.setScaleType(ImageView.ScaleType.CENTER)
        getPicasso().load(getE3SBaseUrl() + url).placeholder(R.drawable.progress_drawable).noFade().into(photoView, object : Callback {
            override fun onSuccess() {
                photoView?.setScaleType(ImageView.ScaleType.CENTER_CROP)
            }

            override fun onError() {
                photoView?.setScaleType(ImageView.ScaleType.CENTER_CROP)
                photoView?.setImageResource(R.drawable.default_avatar)
            }
        })
    }

    fun highlightCity(city: String?): Boolean {
        cityTextView?.setText(city ?: "")

        val eventCity = getEventCity()
        if (city == null || city.isBlank() || eventCity.isBlank() || city.equals(eventCity)) {
            changeBackgroundColor(null)
            cityTextView?.setVisibility(View.INVISIBLE)
            return false
        } else {
            changeBackgroundColor(R.color.warning_color)
            cityTextView?.setVisibility(View.VISIBLE)
            return true
        }
    }

    fun changeBackgroundColor(color: Int?) {
        val resources = getResources()

        if (color == null) {
            getView().setBackgroundColor(Color.WHITE)
            fullNameTextView?.setTextColor(resources.getColor(R.color.sharp_blue))
            titleTextView?.setTextColor(resources.getColor(R.color.medium_gray))
        } else {
            getView().setBackgroundColor(resources.getColor(color))
            fullNameTextView?.setTextColor(Color.WHITE)
            titleTextView?.setTextColor(Color.WHITE)
        }
    }

    fun restoreDefault() {
        fullNameTextView?.setText(R.string.employee_name_default)
        titleTextView?.setText(R.string.employee_title_default)
        photoView?.setImageResource(R.drawable.default_avatar)
        cityTextView?.setText("")
        changeBackgroundColor(null)
    }
}
