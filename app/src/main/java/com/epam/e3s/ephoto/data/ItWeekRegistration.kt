package com.epam.e3s.ephoto.data

data class ItWeekRegistration(val employee: ItWeekRegistration.Employee?, val status: String, val message: String, val alreadyRegistered: Boolean) {
    data class Employee(val name: String, val city: String, val country: String, val title: String, val upsaId: String, val tag: String, val photoUrl: String)
}

