package com.epam.e3s.ephoto

import android.app.ActionBar
import android.app.Fragment
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.WindowManager
import com.epam.e3s.ephoto.service.E3SRestServiceBuilder
import com.epam.e3s.ephoto.service.SettingsService

public class MainActivity : AppCompatActivity() {

    var e3SRestServiceBuilder: E3SRestServiceBuilder? = null
    var settingsService: SettingsService? = null

    var showMenu = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        val actionBar = getSupportActionBar()
        if (actionBar != null) {
            actionBar.setElevation(0f) // remove bottom shadow
            actionBar.setTitle("E3S Security")
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME, ActionBar.DISPLAY_SHOW_CUSTOM)
        }
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            setFragment(SecurityMainFragment(), false)
        }

        initServices()
    }

    private fun initServices() {
        val settings = SettingsService(this)
        settingsService = settings

        val rest = E3SRestServiceBuilder()
        rest.buildE3SRestAdapter(settings.getE3SBaseUrl(), settings.getE3SUser(), settings.getE3SPassword())
        e3SRestServiceBuilder = rest
    }

    val preferenceChangeListener = object : SharedPreferences.OnSharedPreferenceChangeListener {
        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
            when (key) {
                SettingsService.Key.E3S_URL, SettingsService.Key.E3S_USERNAME, SettingsService.Key.E3S_PASSWD -> {
                    e3SRestServiceBuilder!!.buildE3SRestAdapter(settingsService!!.getE3SBaseUrl(), settingsService!!.getE3SUser(), settingsService!!.getE3SPassword())
                }
            }
        }
    }

    override fun onResume() {
        super.onResume();
        settingsService!!.prefs.registerOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    override fun onPause() {
        super.onPause();
        settingsService!!.prefs.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        getMenuInflater().inflate(R.menu.menu_main, menu)
        return showMenu
    }

    fun showMenu(state: Boolean) {
        showMenu = state
        invalidateOptionsMenu()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.getItemId()

        when (id) {
            R.id.settings -> {
                setFragment(SettingsFragment(), true)
                showMenu(false)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val fragmentManager = getFragmentManager()
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack()
            showMenu(true) //TODO dirty hack. It belongs somewhere else
        } else {
            super.onBackPressed()
        }
    }

    fun setFragment(fragment: Fragment?, backStack: Boolean) {
        val fragmentTag = fragment!!.javaClass.getName()
        val t = getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, fragmentTag);
        if (backStack) t.addToBackStack(fragmentTag);
        t.commit();
    }
}
