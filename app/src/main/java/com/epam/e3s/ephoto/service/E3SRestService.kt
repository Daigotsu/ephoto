package com.epam.e3s.ephoto.service

import com.epam.e3s.ephoto.data.ItWeekRegistration
import retrofit.Callback
import retrofit.client.Response
import retrofit.http.GET
import retrofit.http.Path
import retrofit.http.Query


interface E3SRestService {
    GET("/{path}")
    public fun loadPath(Path("path", encode = false) path: String): Response

    GET("/itweek/register")
    public fun registerEmployee(Query("tag") tag: String, Query("hash") hash: String, cb: Callback<ItWeekRegistration>)
}