package com.epam.e3s.ephoto.util

import android.content.res.Resources
import com.google.gson.GsonBuilder
import java.io.BufferedReader
import java.io.InputStreamReader

fun <T> json(resources: Resources, id: Int, type: Class<T>): T {
    val reader = BufferedReader(InputStreamReader(resources.openRawResource(id), "UTF-8"))
    return GsonBuilder().create().fromJson(reader, type)
}