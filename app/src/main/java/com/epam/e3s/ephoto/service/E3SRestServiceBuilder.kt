package com.epam.e3s.ephoto.service

import android.content.Context
import android.util.Base64
import com.epam.e3s.ephoto.BuildConfig
import com.squareup.okhttp.Interceptor
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Response
import com.squareup.picasso.OkHttpDownloader
import com.squareup.picasso.Picasso
import retrofit.RestAdapter
import retrofit.client.OkClient

class E3SRestServiceBuilder {
    private var e3sService: E3SRestService? = null
    private var client: OkHttpClient? = null

    public fun buildE3SRestAdapter(baseUrl: String, username: String, password: String) {
        client = getClient(username, password)

        e3sService = RestAdapter.Builder()
                .setClient(OkClient(client))
                .setLogLevel(if (BuildConfig.DEBUG) RestAdapter.LogLevel.FULL else RestAdapter.LogLevel.NONE)
                .setEndpoint(baseUrl)
                .build()
                .create(javaClass<E3SRestService>())
    }

    public fun isInitialized(): Boolean = e3sService != null

    public fun getE3SRestService(): E3SRestService = e3sService!!; //TODO maybe throw more appropriate exception sometime

    public fun getPicasso(ctx: Context): Picasso = Picasso.Builder(ctx).downloader(OkHttpDownloader(client)).build()

    private fun getToken(username: String, password: String) = Base64.encodeToString("$username:$password".toByteArray(), Base64.DEFAULT)

    private fun getClient(username: String, password: String): OkHttpClient {
        val okHttpClient = OkHttpClient()
        okHttpClient.networkInterceptors().add(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                return chain.proceed(chain.request().newBuilder().header("Authorization", "Basic ${getToken(username, password)}").build())
            }
        })
        return okHttpClient
    }
}
